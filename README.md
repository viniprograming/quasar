# Quasar App (quasar-project)

A Quasar Project

## Instale as dependencias
```bash
yarn
# ou
npm install
```

### Starte o projeto
```bash
yarn dev
# ou
quasar dev
```


### Faça o lint dos arquivos
```bash
yarn lint
# ou
npm run lint
```



### Builde o app para produção
```bash
yarn build
# ou
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
