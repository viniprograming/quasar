import { defineStore } from 'pinia'

interface Todo {
  id: number;
  text: string;
}

export const useTodoStore = defineStore('todo', {
  state: () => ({
    todos: [] as Todo[]
  }),
  getters: {
    getTodos: (state) => state.todos
  },
  actions: {
    addTodo(text: string) {
      const id =
        this.todos.length > 0 ? this.todos[this.todos.length - 1].id + 1 : 1
      this.todos = [...this.todos, { id, text }]
    },
    removeTodo(id: number) {
      this.todos = this.todos.filter((todo) => todo.id !== id)
    },
    updateTodo(todo: Todo) {
      this.todos = this.todos.map((t) => (t.id === todo.id ? todo : t))
    }
  }
})
